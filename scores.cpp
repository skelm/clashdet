/*
 *  scores.cpp
 *
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include "skutil.h"
#include "scores.h"

#define P(EX) cout << #EX << ": " << EX << endl;


using namespace std;



double Scores::distance(const Atom& a, const Atom& b)
{
  return sqrt(pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2));
}

double Scores::square_distance(const Atom& a, const Atom& b)
{
  return pow((a.x - b.x), 2) + pow((a.y - b.y), 2) + pow((a.z - b.z), 2);
}

double Scores::RMSD(const Pdb& a, const Pdb& b)
{
  require(a.size() == b.size(), "Scores::RMSD : Structures need to be of the same length");
  require(a.size() > 0, "Scores::RMSD : Structures need to be longer than 0 residues.");
  
  double sumdist = 0.0;
  for (unsigned int i=0; i<a.size(); i++)
  {
    sumdist += Scores::square_distance(a[i], b[i]);
  }
  return sqrt(sumdist / a.size());
}

