# Clashdet

Copyright by Sebastian Kelm, 2009

This program is a fast C++ implementation of a protein clash detector using a geometric hash table.

The program outputs all clashes found within the given PDB file, with hard-sphere radii set to a user-definable fraction of their elemental Van-der-Waals radii.