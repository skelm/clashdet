/*
 *  scores.h
 *
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#ifndef PROTEINS_H
#define PROTEINS_H

#include <cmath>
#include <vector>
#include "pdb.h"


namespace Scores
{
  double distance(const Atom& a, const Atom& b);
  double square_distance(const Atom& a, const Atom& b);
  double RMSD(const Pdb& a, const Pdb& b);
}


#endif
