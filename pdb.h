/*
 *  pdb.h
 *  Created by Sebastian Kelm on 01/03/2009.
 *
 */

#ifndef PDB_H
#define PDB_H

#include <iostream>
#include <string>
#include <vector>
#include <cmath>


// Representation of a single ATOM in a PDB file
class Atom
{
  public:
    double x, y, z, occup, b;
    int iatom, ires, resindex;
    std::string atom, res, chain, altloc, inscode, element;
    
    Atom() : x(0), y(0), z(0), occup(0), b(0), iatom(0), ires(0), resindex(-1) {};
    Atom(const std::string& line);
    
    void print(std::ostream& out = std::cout) const;
    
    int operator<(const Atom& other) const;
    int operator>(const Atom& other) const;
    Atom& operator=(const Atom& other);
    
    bool is_initialised() const
    {
      return (resindex >= 0);
    }
    
    double& operator[](int i)
    {
      switch (i)
      {
        case 0: return x;
        case 1: return y;
        case 2: return z;
      }
      throw ValueError("Must not index an atom with any integer other than 0,1,2.");
    }
    
    const double& operator[](int i) const
    {
      switch (i)
      {
        case 0: return x;
        case 1: return y;
        case 2: return z;
      }
      throw ValueError("Must not index an atom with any integer other than 0,1,2.");
    }
    
    void set_xyz(double* a)
    {
      x = a[0];
      y = a[1];
      z = a[2];
    }

    void get_xyz(double* a) const
    {
      a[0] = x;
      a[1] = y;
      a[2] = z;
    }
};

class Pdb
{
  public:
    Pdb() : residueCount(0) {};
    Pdb(const Pdb &pdb);
    Pdb(char* name);
    Pdb(std::string name);
    Pdb(std::string name, std::istream& in);

    inline const std::string getName() const {return name;}
    inline const std::vector<Atom>& getAtoms() const {return atoms;}
    
    inline unsigned int size() const {return atoms.size();}
    inline unsigned int countAtoms() const {return atoms.size();}
    unsigned int countResidues();
//     inline Atom& getAtom(int i) {return atoms[i];}
    
    Pdb copyByAtomType(std::string atype) const;
    void splitChains(std::vector<Pdb>& output, std::vector<std::string>& chainids) const;
    
    Atom& operator[](int x);
    const Atom& operator[](int x) const;
    
    inline void print(std::ostream& out = std::cout) const
    {
      for (unsigned int i=0; i<atoms.size(); i++)
        atoms[i].print(out);
    }
    
    void sort();
    //inline void add(Atom &a) {atoms.push_back(a);}
    
    void add(Atom a);
    
    void initAtoms(std::istream& in);

  private:
    std::string name;
    std::vector<Atom> atoms;
    unsigned int residueCount;
    
    Pdb(std::string name, std::vector<Atom>& a, int resCount=-1);
};


inline std::string toString(Atom a)
{
  std::stringstream ss;
  a.print(ss);
  return ss.str();
}
inline std::string toString(Pdb a)
{
  std::stringstream ss;
  a.print(ss);
  return ss.str();
}


double atom_distance(const Atom& a, const Atom& b);


#endif
