CPP = g++
CPPFLAGS = -O2 -Wall -g
OFLAG = -o
.SUFFIXES :
%.o: %.cpp %.h skutil.h
	$(CPP) $(CPPFLAGS) -c $(OFLAG) $@ $<


all: \
	clashdet \
	naive

scratch: \
	clean \
	all

clashdet: skutil.h clashdet.o pdb.h pdb.o scores.h scores.o
	$(CPP) $(CPPFLAGS) $(OFLAG)clashdet clashdet.o pdb.o scores.o

naive: skutil.h naive.o pdb.h pdb.o scores.h scores.o
	$(CPP) $(CPPFLAGS) $(OFLAG)naive naive.o pdb.o scores.o

clean:
	rm -f *.o clashdet naive

