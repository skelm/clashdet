//#define DEBUG
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <sstream>
#include <cstring>
#include <unistd.h>

#include "skutil.h"
#include "pdb.h"
#include "scores.h"

using namespace std;


inline double radius(char c)
{
  switch(c)
  {
    case 'C': return 1.70;
    case 'N': return 1.55;
    case 'O': return 1.52;
    case 'S': return 1.80;
    case 'P': return 1.80;
    case 'H': return 1.20;
    default:
      return 1.70;
  }
}

inline double threshold(const Atom& a, const Atom& b)
{
  if ((a.atom == "SG") && (b.atom == "SG"))
  {
    return 2.0;
  }
  return radius(a.element[0]) + radius(b.element[0]);
}

inline bool clashExcluded(const Atom& a, const Atom& b)
{
  int d = a.resindex - b.resindex;
  if (!(abs(d) <= 1 and a.chain == b.chain))
    return 0;
  return 0 == d or (d < 0 and a.atom  == "C" and b.atom == "N") or (d > 0 and a.atom == "N" and b.atom == "C");
}

inline bool isClash(const Atom& a, const Atom& b, double stringency, double threshold)
{
  return Scores::distance(a, b) < (stringency * threshold);
}


int main(int argc, char* argv[])
{
  double stringency = 0.65;
  bool stop_after_first_clash = false;
  bool counts_only = false;
  
  {
    // Command line option parsing
    
    int c;
    while (-1 != (c = getopt (argc, argv, "hs:bc")))
    {
      switch (c)
      {
        case 'h':
        {
          cerr << "\n"
                  "Clashdet - protein clash detection\n"
                  "\n"
                  "      Copyright by Sebastian Kelm <sebastian.kelm@gmail.com>, 2009.\n"
                  "\n"
                  "      https://bitbucket.org/skelm/clashdet\n"
                  "\n"
                  "USAGE:\n"
                  "\n"
                  "      " << basename(argv[0]) << " [OPTIONS] PDBFILE [PDBFILE ...]\n"
                  "\n"
                  "OPTIONS:\n"
                  "\n"
                  "        -h        Show this help message and exit.\n"
                  "\n"
                  "        -s FRACTION\n"
                  "                  Set precision to the specified fraction of the\n"
                  "                  summed atomic van der Waal's radii of any two atoms.\n"
                  "                  (Default: 0.65)\n"
                  "\n"
                  "        -b        Boolean output: Only return 1 if there is at least one\n"
                  "                  clash, 0 otherwise. This is faster than finding all\n"
                  "                  clashes in a protein.\n"
                  "\n"
                  "        -c        Counts only. Don't give details about each clash.\n"
                  "\n";
          return 1;
        }
        case 's':
        {
          stringency = atof(optarg);
          #ifdef DEBUG
          cerr << "SET OPTION: [" << (char)c << "] Set stringency (fraction): " << stringency << "\n";
          #endif
          require(stringency > 0, "ERROR: stringency must be > 0");
          //require(stringency <= 1, "ERROR: stringency must be <= 1");
          break;
        }
        case 'b':
        {
          #ifdef DEBUG
          cerr << "SET OPTION: [" << (char)c << "] Boolean output (stop after first clash).\n";
          #endif
          stop_after_first_clash = true;
          break;
        }
        case 'c':
        {
          #ifdef DEBUG
          cerr << "SET OPTION: [" << (char)c << "] Counts only (don't give details about each clash).\n";
          #endif
          counts_only = true;
          break;
        }
        case '?':
        {
          if (isprint (optopt))
            fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else
            fprintf (stderr,
                    "Unknown option character `\\x%x'.\n",
                    optopt);
          return 1;
        }
        default:
          abort();
      }
    }
  }
  // End of command line option parsing
  
  if (argc - optind < 1)
  {
    cerr << "USAGE:\n\n"
            "      " << basename(argv[0]) << " [OPTIONS] PDBFILE [PDBFILE ...]\n\n";
    cerr << "ERROR: Provide PDB file(s) as input argument(s).\nUse the -h option to get more help." << endl;
    return 1;
  }
  
  for (int iopt=0; iopt < argc - optind; ++iopt)
  {
    if (argc - optind > 1)
    {
      if (iopt > 0)
      {
        cout << endl;
      }
      cout << "==> " << argv[optind+iopt] << " <==" << endl;
    }
    
    Pdb protein;
    if (strcmp(argv[optind+iopt], "-") == 0)
    {
      protein.initAtoms(cin);
    }
    else
    {
      ifstream instrm(argv[optind+iopt]);
      protein.initAtoms(instrm);
    }
    
    // Naive clash detection algorithm, for comparison and debugging
    //
    unsigned naive_clashes = 0;
    bool stop = false;
    for (unsigned i=0; i<protein.size(); i++)
    {
      if (stop)
      {
        break;
      }
      Atom& a = protein[i];
      for (unsigned j=0; j<protein.size(); j++)
      {
        Atom& b = protein[j];
        if (clashExcluded(a, b))
          continue;
        
        double thres = threshold(a, b);
        if (isClash(a, b, stringency, thres))
        {
          naive_clashes++;
          if (!counts_only)
          {
            cerr << "Clash: distance="<< Scores::distance(a, b) << " cut-off=" << (stringency * thres) << " atom1:" << a.atom << " atom2:" << b.atom << " \n";
            a.print(cerr);
            b.print(cerr);
          }
          if (stop_after_first_clash)
          {
            naive_clashes = 2;
            stop = true;
            break;
          }
        }
      }
    }
    cerr << "Total clashes:  " << naive_clashes / 2 << endl;
  }
}
