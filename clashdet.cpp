//#define DEBUG
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <sstream>
#include <cstring>
#include <unistd.h>

#include "skutil.h"
#include "pdb.h"
#include "scores.h"

using namespace std;


const double unit=0.1; // geometrical unit of 0.1 Angstroms


double bonded_threshold(const Atom& a, const Atom& b)
{
  if (a.resindex != b.resindex or a.res != b.res)
  {
    int d = a.resindex - b.resindex;
    if (a.chain == b.chain and a.inscode.empty() and b.inscode.empty())
    {
      int d2 = a.ires - b.ires;
      if (abs(d2) <= abs(d))
        d = d2;
    }

    // Disulphide bond
    //
    if (d != 0 and a.atom == "SG" and b.atom == "SG")
    {
      return 1.88; // 2.0 is the default length
    }
    // Peptide bond
    //
    if ((d == -1 and a.atom  == "C" and b.atom == "N") or (d == 1 and a.atom == "N" and b.atom == "C"))
    {
      return 1.20; // 1.33 is the default length
    }

    return 99999.0; // These atoms should not be bonded, so we should use the default VdW distance cut-off
  }
  
  string a1, a2, pair;
  if (a.atom < b.atom)
  {
    a1 = a.atom;
    a2 = b.atom;
  }
  else
  {
    a1 = b.atom;
    a2 = a.atom;
  }
  if (a1 == "OXT")
  {
    a1 = "O";
  }
  if (a2 == "OXT")
  {
    a2 = "O";
  }
  pair = a.res + "," + a1 + "," + a2;
  
  double cutoff = 99999.0;

  if (pair == "ALA,C,CA") { cutoff = 1.39; }
  else if (pair == "ALA,C,O") { cutoff = 1.10; }
  else if (pair == "ALA,CA,CB") { cutoff = 1.37; }
  else if (pair == "ALA,CA,N") { cutoff = 1.33; }
  else if (pair == "ARG,C,CA") { cutoff = 1.39; }
  else if (pair == "ARG,C,O") { cutoff = 1.10; }
  else if (pair == "ARG,CA,CB") { cutoff = 1.39; }
  else if (pair == "ARG,CA,N") { cutoff = 1.32; }
  else if (pair == "ARG,CB,CG") { cutoff = 1.35; }
  else if (pair == "ARG,CD,CG") { cutoff = 1.35; }
  else if (pair == "ARG,CD,NE") { cutoff = 1.32; }
  else if (pair == "ARG,CZ,NE") { cutoff = 1.20; }
  else if (pair == "ARG,CZ,NH1") { cutoff = 1.07; }
  else if (pair == "ARG,CZ,NH2") { cutoff = 1.00; }
  else if (pair == "ASN,C,CA") { cutoff = 1.39; }
  else if (pair == "ASN,C,O") { cutoff = 1.10; }
  else if (pair == "ASN,CA,CB") { cutoff = 1.39; }
  else if (pair == "ASN,CA,N") { cutoff = 1.32; }
  else if (pair == "ASN,CB,CG") { cutoff = 1.36; }
  else if (pair == "ASN,CG,ND2") { cutoff = 1.18; }
  else if (pair == "ASN,CG,OD1") { cutoff = 1.09; }
  else if (pair == "ASP,C,CA") { cutoff = 1.39; }
  else if (pair == "ASP,C,O") { cutoff = 1.10; }
  else if (pair == "ASP,CA,CB") { cutoff = 1.40; }
  else if (pair == "ASP,CA,N") { cutoff = 1.33; }
  else if (pair == "ASP,CB,CG") { cutoff = 1.36; }
  else if (pair == "ASP,CG,OD1") { cutoff = 1.11; }
  else if (pair == "ASP,CG,OD2") { cutoff = 1.11; }
  else if (pair == "CYS,C,CA") { cutoff = 1.38; }
  else if (pair == "CYS,C,O") { cutoff = 1.10; }
  else if (pair == "CYS,CA,CB") { cutoff = 1.39; }
  else if (pair == "CYS,CA,N") { cutoff = 1.32; }
  else if (pair == "CYS,CB,SG") { cutoff = 1.64; }
  else if (pair == "GLN,C,CA") { cutoff = 1.39; }
  else if (pair == "GLN,C,O") { cutoff = 1.10; }
  else if (pair == "GLN,CA,CB") { cutoff = 1.39; }
  else if (pair == "GLN,CA,N") { cutoff = 1.32; }
  else if (pair == "GLN,CB,CG") { cutoff = 1.35; }
  else if (pair == "GLN,CD,CG") { cutoff = 1.36; }
  else if (pair == "GLN,CD,NE2") { cutoff = 1.18; }
  else if (pair == "GLN,CD,OE1") { cutoff = 1.08; }
  else if (pair == "GLU,C,CA") { cutoff = 1.39; }
  else if (pair == "GLU,C,O") { cutoff = 1.10; }
  else if (pair == "GLU,CA,CB") { cutoff = 1.40; }
  else if (pair == "GLU,CA,N") { cutoff = 1.32; }
  else if (pair == "GLU,CB,CG") { cutoff = 1.36; }
  else if (pair == "GLU,CD,CG") { cutoff = 1.36; }
  else if (pair == "GLU,CD,OE1") { cutoff = 1.09; }
  else if (pair == "GLU,CD,OE2") { cutoff = 1.06; }
  else if (pair == "GLY,C,CA") { cutoff = 1.34; }
  else if (pair == "GLY,C,O") { cutoff = 1.10; }
  else if (pair == "GLY,CA,N") { cutoff = 1.32; }
  else if (pair == "HIS,C,CA") { cutoff = 1.39; }
  else if (pair == "HIS,C,O") { cutoff = 1.10; }
  else if (pair == "HIS,CA,CB") { cutoff = 1.39; }
  else if (pair == "HIS,CA,N") { cutoff = 1.32; }
  else if (pair == "HIS,CB,CG") { cutoff = 1.36; }
  else if (pair == "HIS,CD2,CE1") { cutoff = 2.05; }
  else if (pair == "HIS,CD2,CG") { cutoff = 1.23; }
  else if (pair == "HIS,CD2,NE2") { cutoff = 1.25; }
  else if (pair == "HIS,CE1,CG") { cutoff = 2.06; }
  else if (pair == "HIS,CE1,ND1") { cutoff = 1.20; }
  else if (pair == "HIS,CE1,NE2") { cutoff = 1.20; }
  else if (pair == "HIS,CG,ND1") { cutoff = 1.25; }
  else if (pair == "ILE,C,CA") { cutoff = 1.39; }
  else if (pair == "ILE,C,O") { cutoff = 1.10; }
  else if (pair == "ILE,CA,CB") { cutoff = 1.40; }
  else if (pair == "ILE,CA,N") { cutoff = 1.33; }
  else if (pair == "ILE,CB,CG1") { cutoff = 1.39; }
  else if (pair == "ILE,CB,CG2") { cutoff = 1.38; }
  else if (pair == "ILE,CD1,CG1") { cutoff = 1.32; }
  else if (pair == "LEU,C,CA") { cutoff = 1.39; }
  else if (pair == "LEU,C,O") { cutoff = 1.10; }
  else if (pair == "LEU,CA,CB") { cutoff = 1.40; }
  else if (pair == "LEU,CA,N") { cutoff = 1.32; }
  else if (pair == "LEU,CB,CG") { cutoff = 1.39; }
  else if (pair == "LEU,CD1,CG") { cutoff = 1.37; }
  else if (pair == "LEU,CD2,CG") { cutoff = 1.37; }
  else if (pair == "LYS,C,CA") { cutoff = 1.39; }
  else if (pair == "LYS,C,O") { cutoff = 1.10; }
  else if (pair == "LYS,CA,CB") { cutoff = 1.40; }
  else if (pair == "LYS,CA,N") { cutoff = 1.32; }
  else if (pair == "LYS,CB,CG") { cutoff = 1.33; }
  else if (pair == "LYS,CD,CE") { cutoff = 1.33; }
  else if (pair == "LYS,CD,CG") { cutoff = 1.35; }
  else if (pair == "LYS,CE,NZ") { cutoff = 1.31; }
  else if (pair == "MET,C,CA") { cutoff = 1.39; }
  else if (pair == "MET,C,O") { cutoff = 1.10; }
  else if (pair == "MET,CA,CB") { cutoff = 1.39; }
  else if (pair == "MET,CA,N") { cutoff = 1.32; }
  else if (pair == "MET,CB,CG") { cutoff = 1.34; }
  else if (pair == "MET,CE,SD") { cutoff = 1.49; }
  else if (pair == "MET,CG,SD") { cutoff = 1.55; }
  else if (pair == "PHE,C,CA") { cutoff = 1.39; }
  else if (pair == "PHE,C,O") { cutoff = 1.10; }
  else if (pair == "PHE,CA,CB") { cutoff = 1.40; }
  else if (pair == "PHE,CA,N") { cutoff = 1.32; }
  else if (pair == "PHE,CB,CG") { cutoff = 1.36; }
  else if (pair == "PHE,CD1,CE1") { cutoff = 1.24; }
  else if (pair == "PHE,CD1,CG") { cutoff = 1.25; }
  else if (pair == "PHE,CD2,CE2") { cutoff = 1.24; }
  else if (pair == "PHE,CD2,CG") { cutoff = 1.25; }
  else if (pair == "PHE,CE1,CZ") { cutoff = 1.23; }
  else if (pair == "PHE,CE2,CZ") { cutoff = 1.23; }
  else if (pair == "PRO,C,CA") { cutoff = 1.39; }
  else if (pair == "PRO,C,O") { cutoff = 1.10; }
  else if (pair == "PRO,CA,CB") { cutoff = 1.40; }
  else if (pair == "PRO,CA,N") { cutoff = 1.33; }
  else if (pair == "PRO,CB,CG") { cutoff = 1.31; }
  else if (pair == "PRO,CD,CG") { cutoff = 1.34; }
  else if (pair == "PRO,CD,N") { cutoff = 1.34; }
  else if (pair == "SER,C,CA") { cutoff = 1.39; }
  else if (pair == "SER,C,O") { cutoff = 1.10; }
  else if (pair == "SER,CA,CB") { cutoff = 1.39; }
  else if (pair == "SER,CA,N") { cutoff = 1.32; }
  else if (pair == "SER,CB,OG") { cutoff = 1.27; }
  else if (pair == "THR,C,CA") { cutoff = 1.39; }
  else if (pair == "THR,C,O") { cutoff = 1.10; }
  else if (pair == "THR,CA,CB") { cutoff = 1.39; }
  else if (pair == "THR,CA,N") { cutoff = 1.32; }
  else if (pair == "THR,CB,CG2") { cutoff = 1.37; }
  else if (pair == "THR,CB,OG1") { cutoff = 1.30; }
  else if (pair == "TRP,C,CA") { cutoff = 1.37; }
  else if (pair == "TRP,C,O") { cutoff = 1.10; }
  else if (pair == "TRP,CA,CB") { cutoff = 1.39; }
  else if (pair == "TRP,CA,N") { cutoff = 1.32; }
  else if (pair == "TRP,CB,CG") { cutoff = 1.35; }
  else if (pair == "TRP,CD1,CG") { cutoff = 1.23; }
  else if (pair == "TRP,CD1,NE1") { cutoff = 1.24; }
  else if (pair == "TRP,CD2,CE2") { cutoff = 1.28; }
  else if (pair == "TRP,CD2,CE3") { cutoff = 1.27; }
  else if (pair == "TRP,CD2,CG") { cutoff = 1.30; }
  else if (pair == "TRP,CE2,CZ2") { cutoff = 1.26; }
  else if (pair == "TRP,CE2,NE1") { cutoff = 1.25; }
  else if (pair == "TRP,CE3,CZ3") { cutoff = 1.24; }
  else if (pair == "TRP,CH2,CZ2") { cutoff = 1.24; }
  else if (pair == "TRP,CH2,CZ3") { cutoff = 1.26; }
  else if (pair == "TYR,C,CA") { cutoff = 1.39; }
  else if (pair == "TYR,C,O") { cutoff = 1.10; }
  else if (pair == "TYR,CA,CB") { cutoff = 1.39; }
  else if (pair == "TYR,CA,N") { cutoff = 1.32; }
  else if (pair == "TYR,CB,CG") { cutoff = 1.37; }
  else if (pair == "TYR,CD1,CE1") { cutoff = 1.24; }
  else if (pair == "TYR,CD1,CG") { cutoff = 1.26; }
  else if (pair == "TYR,CD2,CE2") { cutoff = 1.24; }
  else if (pair == "TYR,CD2,CG") { cutoff = 1.25; }
  else if (pair == "TYR,CE1,CZ") { cutoff = 1.24; }
  else if (pair == "TYR,CE2,CZ") { cutoff = 1.24; }
  else if (pair == "TYR,CZ,OH") { cutoff = 1.24; }
  else if (pair == "VAL,C,CA") { cutoff = 1.39; }
  else if (pair == "VAL,C,O") { cutoff = 1.10; }
  else if (pair == "VAL,CA,CB") { cutoff = 1.40; }
  else if (pair == "VAL,CA,N") { cutoff = 1.32; }
  else if (pair == "VAL,CB,CG1") { cutoff = 1.37; }
  else if (pair == "VAL,CB,CG2") { cutoff = 1.37; }

  return cutoff;
}


inline double radius(const Atom& a)
{
  if (a.res.find("CU") == 0 and a.atom.find("CU") == 0) return 1.4;
  if (a.res.find("CL") == 0 and a.atom.find("CL") == 0) return 1.75;
  if (a.res.find("SE") == 0 and a.atom.find("SE") == 0) return 1.9;
  if (a.res.find("TE") == 0 and a.atom.find("TE") == 0) return 2.06;
  if (a.res.find("BR") == 0 and a.atom.find("BR") == 0) return 1.85;
  if (a.res.find("ZN") == 0 and a.atom.find("ZN") == 0) return 1.40;

  char c = a.element[0];
  switch(c)
  {
    case 'C': return 1.70;
    case 'N': return 1.55;
    case 'O': return 1.52;
    case 'S': return 1.80;
    case 'P': return 1.80;
    case 'H': return 1.20;
    case 'F': return 1.47;
    case 'I': return 1.98;
    default:
      return 1.40;
      //throw ValueError("GeometricHash::radius() : Illegal atom type: "+c);
  }
}


inline double threshold(const Atom& a, const Atom& b, double stringency)
{
  double cutoff = bonded_threshold(a, b);
  if (cutoff < 100.0)
  {
    return cutoff; // we've got a specific cut-off for covalently bonded atoms
  }
  cutoff = stringency * (radius(a) + radius(b));
  if (a.resindex == b.resindex)
  {
    return cutoff - 0.1; // be slightly looser on non-bonded cut-offs within the same residue
  }
  return cutoff; // standard behaviour for non-bonded atoms in different residues
}


inline bool clashExcluded(const Atom& a, const Atom& b)
{
  const string AMINOACIDS = ",ALA,ARG,ASN,ASP,CYS,GLN,GLU,GLY,HIS,ILE,LEU,LYS,MET,PHE,PRO,SER,THR,TRP,TYR,VAL,";
  if (a.resindex == b.resindex)
  {
    if (a.atom == b.atom or (!a.altloc.empty() and !b.altloc.empty() and a.altloc != b.altloc))
    {
      return true; // These atoms are alternative locations, so ignore any clashes between them
    }
    if (AMINOACIDS.find(","+a.res+",") == string::npos)
    {
      return true; // This is an internal comparison of an unknown residue type. We don't have any bonding information, so just ignore this
    }
  }
  return false;
}



////////////////////////////////////////////////////////////////////////////////


template<typename T>
class Matrix3D
{
  size_t dimX, dimY, dimZ;
  vector<T> data;
//   T *data;

  public:
    Matrix3D()
    : dimX(0), dimY(0), dimZ(0)
    {
    }
    
    Matrix3D(size_t x, size_t y, size_t z, T defVal)
    : dimX(x), dimY(y), dimZ(z), data(x*y*z, defVal)
    {
      if (x==0 or y==0 or z==0)
        throw OutOfBounds("Matrix3D::Matrix3D() : x,y,z dimensions must not be 0");
    }
    
/*
    Matrix3D(const Matrix3D& a)
    : dimX(a.dimX), dimY(a.dimY), dimZ(a.dimZ), data(a.data)
    {
    }
    
    &Matrix3D operator= (const Matrix3D& a)
    {
      dimX = a.dimX;
      dimY = a.dimY;
      dimZ = a.dimZ;
      data = a.data;
    }
*/

    inline size_t size() const
    {
      return data.size();
    }
    inline size_t X() const
    {
      return dimX;
    }
    inline size_t Y() const
    {
      return dimY;
    }
    inline size_t Z() const
    {
      return dimZ;
    }
    
    inline T& operator()(size_t x, size_t y, size_t z)
    {
      if (z>=dimZ or y>=dimY or x>=dimX)
        throw OutOfBounds("Matrix3D::operator() : Index out of bounds "+toString(x)+","+toString(y)+","+toString(y));
      return data[x + dimX*y + dimX*dimY*z];
    }
    inline const T& operator()(size_t x, size_t y, size_t z) const
    {
      if (z>=dimZ or y>=dimY or x>=dimX)
        throw OutOfBounds("Matrix3D::operator() : Index out of bounds "+toString(x)+","+toString(y)+","+toString(y));
      return data[x + dimX*y + dimX*dimY*z];
    }
    
    inline T& operator[](size_t i)
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix3D::operator[] : Index out of bounds "+toString(i));
      return data[i];
    }
    inline const T& operator[](size_t i) const
    {
      if (i>=data.size())
        throw OutOfBounds("Matrix3D::operator[] : Index out of bounds "+toString(i));
      return data[i];
    }
};



class GeometricHash
{
  const Pdb* protein;
  Matrix3D<const Atom*> data;
  unsigned long dupFlag;
  map<size_t, pair<size_t, const Atom**> > dups;

//   vector<const Atom*> data;
  //map<int, Atom*> duplicates; // if several atoms occupy the same space, this is where we store the array pointer they will go.
  
  unsigned binWidth;
  
  // raw PDB data
  double minX, minY, minZ;
  double maxX, maxY, maxZ;

  double stringency;
  bool quiet;

  public:

    class StericClash : public ValueError
    {
      public:
        StericClash(const std::string& s)
        : ValueError(s)
        {
        }
    };


    bool static isClash(GeometricHash& self, const Atom& a, const Atom& b)
    {
      if (clashExcluded(a, b))
      {
        return false;
      }
      return Scores::distance(a, b) < threshold(a, b, self.stringency);
    }

    
    GeometricHash(const Pdb& prot, double optimalDistance=3.6)
    : protein(&prot), dupFlag((unsigned long)&prot), binWidth((unsigned)(1+optimalDistance/unit/2)), minX(0), minY(0), minZ(0), maxX(0), maxY(0), maxZ(0), stringency(1.0), quiet(0)
    {
      init(prot);
      require(binWidth>0, "GeometricHash : binWidth must be > 0 ! Please provide a positive optimalDistance parameter!");
      #ifdef DEBUG
      unsigned arraysCreated=0;
      unsigned arraysDeleted=0;
      #endif
      for (size_t i=0; i<prot.size(); i++)
      {
        size_t index = idx(prot[i]);
        //cerr << "Loading atom ";
        //prot[i].print(cerr);
        //cerr << "\n" << "Index: " << index << "\n";
        if (data[index] != NULL)
        {
          if ((unsigned long)data[index] != dupFlag)
          {
            const Atom** arr = new const Atom*[2];
            arr[0] = data[index];
            arr[1] = &(prot[i]);
            #ifdef DEBUG
            cerr << "Replacing single pointer with 2-array:\nFrom:\n\t";
            data[index]->print(cerr);
            #endif
            dups[index] = pair<size_t, const Atom**>((size_t)2, arr);
            data[index] = (const Atom*) dupFlag;
            #ifdef DEBUG
            cerr << "To:\n";
            pair<size_t, const Atom**>& mydup = dups[index];
            for (size_t j=0; j<mydup.first; j++)
            {
              cerr << "\t";
              mydup.second[j]->print(cerr);
            }
            cerr << endl;
            #endif
          }
          else
          {
            pair<size_t, const Atom**>& mydup = dups[index];
            const Atom** arr = new const Atom*[mydup.first+1];
            for (size_t j=0; j<mydup.first; j++)
              arr[j] = mydup.second[j];
            //memcpy(arr, mydup.second, sizeof(mydup.second));
            #ifdef DEBUG
            cerr << "Extending array:\nFrom:\n";
            for (size_t j=0; j<mydup.first; j++)
            {
              cerr << "\t";
              mydup.second[j]->print(cerr);
            }
            #endif
            arr[mydup.first] = &(prot[i]);
            ++mydup.first;
            delete [] mydup.second;
            mydup.second = arr;
            #ifdef DEBUG
            cerr << "To:\n";
            for (size_t j=0; j<mydup.first; j++)
            {
              cerr << "\t";
              mydup.second[j]->print(cerr);
            }
            cerr << endl;
            arraysDeleted++;
            #endif
          }
          #ifdef DEBUG
          arraysCreated++;
          #endif
          //throw StericClash("Steric clash detected while assigning atoms to GeometricHash. Multiple atoms at same 3D Matrix index:\n"+toString(prot[i])+toString(*(data[index])));
        }
        else
        {
          data[index] = &(prot[i]);
        }
      }

      #ifdef DEBUG
      if (arraysCreated)
        cerr << "Arrays created: " << arraysCreated << "\n";
      if (arraysDeleted)
        cerr << "Arrays deleted: " << arraysDeleted << "\n";
      #endif
    }
   
    ~GeometricHash()
    {
      map<size_t, pair<size_t, const Atom**> >::iterator it = dups.begin();
      for (; it != dups.end(); ++it)
      {
        delete [] it->second.second;
      }
    }
    
    inline void setStringency(double d)
    {
      require(d>0.0, "Illigal stringency: "+toString(d));
      stringency = d;
    }


    inline void setQuiet(bool b)
    {
      quiet = b;
    }


    inline unsigned hasClashes(double maxRadius=3.6, bool stopAtFirst=1)
    {
      Matrix3D<unsigned> sphere = distMatrix(maxRadius);
      
      unsigned clashes = 0;
      size_t size = protein->size();
      for (size_t i=0; i<size; i++)
      {
        clashes += doCube((*protein)[i], &GeometricHash::isClash, maxRadius, sphere, stopAtFirst);
        //clashes += doSphere((*protein)[i], &GeometricHash::isClash, maxRadius, sphere, stopAtFirst);
        if (stopAtFirst and clashes)
          return clashes;
      }
      return clashes / 2;
    }

    
  private:
    inline void init(const Pdb& prot)
    {
      if (prot.size() == 0)
        return;

      double miX, miY, miZ, maX, maY, maZ;
      miX = maX = prot[0].x;
      miY = maY = prot[0].y;
      miZ = maZ = prot[0].z;
      
      for (size_t i=1; i<prot.size(); i++)
      {
        const Atom& a = prot[i];
        if (a.x < miX)
          miX = a.x;
        if (a.x > maX)
          maX = a.x;
        if (a.y < miY)
          miY = a.y;
        if (a.y > maY)
          maY = a.y;
        if (a.z < miZ)
          miZ = a.z;
        if (a.z > maZ)
          maZ = a.z;
      }
      minX = miX;
      minY = miY;
      minZ = miZ;
      maxX = maX;
      maxY = maY;
      maxZ = maZ;
      size_t lenX = 1 + idxX(maX);
      size_t lenY = 1 + idxY(maY);
      size_t lenZ = 1 + idxZ(maZ);
      data = Matrix3D<const Atom*>(lenX, lenY, lenZ, NULL);
      #ifdef DEBUG
      //cerr << "minX: " << minX << "\n";
      //cerr << "minY: " << minY << "\n";
      //cerr << "minZ: " << minZ << "\n";
      //cerr << "maxX: " << maxX << "\n";
      //cerr << "maxY: " << maxY << "\n";
      //cerr << "maxZ: " << maxZ << "\n";
      cerr << "lenX: " << lenX << "\n";
      cerr << "lenY: " << lenY << "\n";
      cerr << "lenZ: " << lenZ << "\n";
      cerr << "data size: " << data.size() << "\n";
      #endif
    }

    inline size_t idxX(double x)
    {
      return (size_t)((x - minX) / (unit * binWidth));
    }
    inline size_t idxY(double y)
    {
      return (size_t)((y - minY) / (unit * binWidth));
    }
    inline size_t idxZ(double z)
    {
      return (size_t)((z - minZ) / (unit * binWidth));
    }
    inline size_t idx(size_t x, size_t y, size_t z)
    {
      return x + (data.X()*y) + (data.X()*data.Y()*z);
    }
    inline size_t idx(const Atom& a)
    {
      return (idxX(a.x)) + (data.X()*idxY(a.y)) + (data.X()*data.Y()*idxZ(a.z));
    }
    inline void idx(const Atom& a, int& x, int& y, int& z)
    {
      x = idxX(a.x);
      y = idxY(a.y);
      z = idxZ(a.z);
    }
    inline void idx(const Atom& a, size_t& x, size_t& y, size_t& z)
    {
      x = idxX(a.x);
      y = idxY(a.y);
      z = idxZ(a.z);
    }
    
    

    inline static double lennard_jones(double vdw6, double dist, double well=100.0)
    {
      // vdw6 is the 6th power of the distance between the two atoms where the lennard-jones
      // potential is 0.
      // dist is the actual distance between the two atoms.
      // well is the depth of the potential well (a constant)
      return 4.0 * well * (dist/(vdw6*vdw6)) - dist/vdw6;
    }
    

    inline Matrix3D<unsigned> distMatrix(double radius_angstrom)
    {
      unsigned radius = 1 + (unsigned)(radius_angstrom/(unit*binWidth));
      Matrix3D<unsigned> m(1+radius, 1+radius, 1+radius, 0);
      
      for(size_t dx=0; dx<=radius; dx++)
      {
        for(size_t dy=0; dy<=radius; dy++)
        {
          for(size_t dz=0; dz<=radius; dz++)
          {
            m(dx, dy, dz) = (unsigned)dist(dx-1, dy-1, dz-1);
          }
        }
      }

      return m;
    }

    
    unsigned doCube(const Atom& a, bool (*action)(GeometricHash&, const Atom&, const Atom&), double radius, const Matrix3D<unsigned>& distMat, bool stopOnSuccess=1)
    {
      long r = 1 + (long)(radius/(unit*binWidth));
      unsigned count=0;
      long x=idxX(a.x);
      long y=idxY(a.y);
      long z=idxZ(a.z);
      
      //cerr << "x,y,z = " <<x<<","<<y<<","<<z<<"\n";
      //cerr << "X,Y,Z = " <<data.X()<<","<<data.Y()<<","<<data.Z()<<"\n";
      
      long loX = max(x-r, (long)0);
      long loY = max(y-r, (long)0);
      long loZ = max(z-r, (long)0);
      long hiX = min(x+r, (long)(data.X()-1));
      long hiY = min(y+r, (long)(data.Y()-1));
      long hiZ = min(z+r, (long)(data.Z()-1));
      
      

      //cerr << "for(size_t ix="<<loX<<"; ix<="<<hiX<<"; ix++)\n";
      //cerr << "\tfor(size_t iy="<<loY<<"; iy<="<<hiY<<"; iy++)\n";
      //cerr << "\t\tfor(size_t iz="<<loZ<<"; iz<="<<hiZ<<"; iz++)\n";
      for(long ix=loX; ix<=hiX; ix++)
      {
        for(long iy=loY; iy<=hiY; iy++)
        {
          for(long iz=loZ; iz<=hiZ; iz++)
          {
            if (distMat(abs(ix-x), abs(iy-y), abs(iz-z)) <= r)
            {
              count += doStuff(ix, iy, iz, a, action, stopOnSuccess);
              if (stopOnSuccess and count)
                return count;
            }
          }
        }
      }
      
      return count;
    }
    
    
    // Iterates through a range, e.g. for(i=0; i<4; absinc(i)) ... like so: 0, 1, -1, 2, -2, 3, -3, 4
    inline void absinc(long& i)
    {
      if (i<=0)
        i=-i+1;
      else
        i = -i;
    }

    
    inline double dist(long dx, long dy, long dz)
    {
      return sqrt(dx*dx + dy*dy + dz*dz);
    }


    inline unsigned doStuff(long x, long y, long z, const Atom& a, bool (*action)(GeometricHash&, const Atom&, const Atom&), bool stopOnSuccess)
    {
      //cerr << "Checking index: " << x <<", "<< y <<", "<< z << "\n";
      const Atom* b;
      //try
      //{
        b = data(x, y, z);
      //}
      //catch(OutOfBounds)
      //{
      //  //cerr << "OutOfBounds\n";
      //  return 0;
      //}
      
      if (b == NULL)
      {
        //cerr << "NULL\n";
        return 0;
      }

      if ((unsigned long)b != dupFlag)
      {
        //cerr << (unsigned long)b << " != " << dupFlag << "\n";
        //cerr << "Singleton\n";
        if (action(*this, a, *b))
        {
          if (!quiet)
          {
            cout << "Clash: distance="<< Scores::distance(a, *b) << " cut-off=" << threshold(a, *b, stringency) << " atom1:" << a.atom << " atom2:" << b->atom << " \n";
            a.print(cout);
            b->print(cout);
              }
          return 1;
        }
        else
          return 0;
      }
      else
      {
        //cerr << "Dup\n";
        unsigned count=0;
        pair<size_t, const Atom**>& dup = dups[idx(x, y, z)];
        //cerr << "First atom in bin: " << dup.first << " " << toString(*(dup.second));
        for (size_t i=0; i<dup.first; i++)
        {
          b = dup.second[i];
          if (action(*this, a, *b))
          {
            if (!quiet)
            {
              cout << "Clash: distance="<< Scores::distance(a, *b) << " cut-off=" << threshold(a, *b, stringency) << " atom1:" << a.atom << " atom2:" << b->atom << " \n";
              a.print(cout);
              b->print(cout);
            }
            if (stopOnSuccess)
              return 1;
            count++;
          }
        }
        return count;
      }
    }

};



////////////////////////////////////////////////////////////////////////////////


int main(int argc, char* argv[])
{
  double stringency = 0.63; // This corresponds more or less to the cut-off used by PyMOL
  bool stop_after_first_clash = false;
  bool counts_only = false;
  
  {
    // Command line option parsing
    
    int c;
    while (-1 != (c = getopt(argc, argv, "hs:bc")))
    {
      switch (c)
      {
        case 'h':
        {
          cerr << "\n"
                  "Clashdet - protein clash detection\n"
                  "\n"
                  "      Copyright by Sebastian Kelm <sebastian.kelm@gmail.com>, 2009.\n"
                  "\n"
                  "      https://bitbucket.org/skelm/clashdet\n"
                  "\n"
                  "USAGE:\n"
                  "\n"
                  "      " << basename(argv[0]) << " [OPTIONS] PDBFILE [PDBFILE ...]\n"
                  "\n"
                  "OPTIONS:\n"
                  "\n"
                  "        -h        Show this help message and exit.\n"
                  "\n"
                  "        -s FRACTION\n"
                  "                  Set precision to the specified fraction of the\n"
                  "                  summed atomic van der Waal's radii of any two atoms.\n"
                  "                  (Default: 0.63)\n"
                  "\n"
                  "        -b        Boolean output: Only return 1 if there is at least one\n"
                  "                  clash, 0 otherwise. This is faster than finding all\n"
                  "                  clashes in a protein.\n"
                  "\n"
                  "        -c        Counts only. Don't give details about each clash.\n"
                  "\n";
          return 1;
        }
        case 's':
        {
          stringency = atof(optarg);
          #ifdef DEBUG
          cerr << "SET OPTION: [" << (char)c << "] Set stringency (fraction): " << stringency << "\n";
          #endif
          require(stringency > 0, "ERROR: stringency must be > 0");
          //require(stringency <= 1, "ERROR: stringency must be <= 1");
          break;
        }
        case 'b':
        {
          #ifdef DEBUG
          cerr << "SET OPTION: [" << (char)c << "] Boolean output (stop after first clash).\n";
          #endif
          stop_after_first_clash = true;
          break;
        }
        case 'c':
        {
          #ifdef DEBUG
          cerr << "SET OPTION: [" << (char)c << "] Counts only (don't give details about each clash).\n";
          #endif
          counts_only = true;
          break;
        }
        case '?':
        {
          if (isprint (optopt))
            fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else
            fprintf (stderr,
                    "Unknown option character `\\x%x'.\n",
                    optopt);
          return 1;
        }
        default:
          abort();
      }
    }
  }
  // End of command line option parsing
  
  if (argc - optind < 1)
  {
    cerr << "USAGE:\n\n"
            "      " << basename(argv[0]) << " [OPTIONS] PDBFILE [PDBFILE ...]\n\n";
    cerr << "ERROR: Provide PDB file(s) as input argument(s).\nUse the -h option to get more help." << endl;
    return 1;
  }
  
  for (int iopt=0; iopt < argc - optind; ++iopt)
  {
    if ((argc - optind > 1) and !counts_only)
    {
      if (iopt > 0)
      {
        cout << endl;
      }
      cout << "==> " << argv[optind+iopt] << " <==" << endl;
    }
    
    Pdb protein;
    if (strcmp(argv[optind+iopt], "-") == 0)
    {
      protein.initAtoms(cin);
    }
    else
    {
      ifstream instrm(argv[optind+iopt]);
      protein.initAtoms(instrm);
    }
    
    //cerr << "Initializing geo...\n";
    
    // GeometricHash(protein, radius) :
    //  Radius is an optimisation parameter.
    //  It should reflect the maximum radius around each atom,
    //  which you expect to work at most of the time
    //  (e.g. for clash detection, use the
    //    vdW radius of carbon * 2).
    //
    GeometricHash geo(protein, 3.6);
    
    // Multiply all vdW radii with this number,
    // when doing clash detection.
    //
    geo.setStringency(stringency);
    
    geo.setQuiet(counts_only);
    
    //cerr << "Done initializing geo.\nNow checking for clashes...\n";
    
    // geo.hasClashes(radius, stopAtFirst);
    // Do clash detection. Count clashes.
    // Radius is the maximum radius (in Angstroms) around every atom to consider.
    // When stopAtFirst is true, the method will return when it finds the first clash.
    // Otherwise, it will count the total number of clashes in the molecule.
    //
    
    unsigned clashes = geo.hasClashes(3.6*stringency, stop_after_first_clash);
    
    if (!(counts_only and (argc - optind > 1)))
    {
      cout << "Total clashes:  " << clashes << endl;
    }
    else
    {
      cout << argv[optind+iopt] << ":" << clashes << endl;
    }
  }
}


